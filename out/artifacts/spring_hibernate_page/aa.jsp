<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"%>
<html>
<head>
    <title>Title</title>
      <link rel="stylesheet" href="static/css/bootstrap.min.css" >
      <link rel="stylesheet" href="static/css/bootstrap-theme.min.css" >
      <link rel="stylesheet" href="static/css/mystyle.css" >
      <script src="static/js/bootstrap.min.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<body>
<form method="post" class="form-control" action="login">

    <div class="panel panel-default" style="width: 500px;margin:auto">
        <div class="panel-heading">
            <h1>注册登录</h1>
        </div>
        <div class="panel-body">
            <div>
                <div class="col-sm-4"> 登录名</div>
                <div class="col-sm-6">
                    <input name="name" placeholder="请输入姓名"><span>${error.name}</span><br>
                </div>
            </div>
            <div>
                <div class="col-sm-4"> 密码</div>
                <div class="col-sm-6">
                    <input name="password" placeholder="请输入密码"><span>${error.password}</span><br>
                </div>
            </div>
        </div>
        <div class="panel-footer" style="text-align: right">
            <button class="btn btn-primary" type="submit">登录</button>
            <button class="btn btn-danger">注册</button>
        </div>
    </div>
</form>
<div>


</div>


</body>
</html>
