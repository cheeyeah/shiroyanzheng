package com.test.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/wx")
public class WxController {

    @RequestMapping( "/ok")
    public Object wx() {
        return "wx";
    }

    @RequestMapping("/wxlogin")
    public Object wxlogin() {
        return "wxlogin";
    }


}
