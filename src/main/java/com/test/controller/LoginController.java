package com.test.controller;

import com.test.dao.AtmDao;
import com.test.entity.AtmEntity;
import com.test.entity.UserEntity;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class LoginController {


    @RequestMapping("login")
    public Object login() {
        return "login";
    }

    @RequestMapping("unauth")
    public Object unauth() {
        return "unauth";
    }

    @RequestMapping("index")
    public Object index() {
        return "index";
    }

    @RequestMapping("loginout")
    public Object loginout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "redirect:index";
    }


    @RequestMapping("logindo")
    public String logindo(String name, String password, Model model) {
        System.out.println("name=" + name + ", password=" + password);
        Subject subject = SecurityUtils.getSubject();
        boolean ok = true;
        if (!subject.isAuthenticated()) {
            UsernamePasswordToken upt = new UsernamePasswordToken(name, password);
            try {
                subject.login(upt);
            } catch (UnknownAccountException e) {
                System.out.println("用户名不存在");
                ok = false;
            } catch (IncorrectCredentialsException e) {
                System.out.println("密码错误");
                ok = false;
            }
        }
        if (ok) {
            return "redirect:index";
        } else {
            model.addAttribute("message", "用户名或密码错误！");
            return "login";
        }

    }

}
