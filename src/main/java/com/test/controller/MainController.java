package com.test.controller;

import com.test.dao.AtmDao;
import com.test.dao.BookDao;
import com.test.entity.AtmEntity;
import com.test.entity.BookEntity;
import com.test.entity.Page;
import com.test.service.PageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

import java.util.List;

@Controller
public class MainController {

    private PageService ps = new PageService();
    AtmDao atmdao=new AtmDao();
    BookDao bookDao=new BookDao();



    @RequestMapping("/book")
    public Object Book(HttpServletRequest request, HttpServletResponse response,Model model) {
              try {
            String pageNo = request.getParameter("pageNo");
            if (pageNo == null) {
                pageNo = "1";
            }
            Page page = ps.queryForBookPage(Integer.valueOf(pageNo), 10);
            request.setAttribute("page", page);
            List<BookEntity> book = page.getList();
            model.addAttribute("book",book);
            model.addAttribute("url","book");
            System.out.println("Model ="+model);

        } catch (Exception e) {
            e.printStackTrace();
        }
             return "list";

    }




    @RequestMapping("/atm")
    public Object Atm(HttpServletRequest request, HttpServletResponse response,Model model) {
        try {
            String pageNo = request.getParameter("pageNo");
            if (pageNo == null) {
                pageNo = "1";
            }
            Page page = ps.queryForAtmPage(Integer.valueOf(pageNo), 10);
            request.setAttribute("page", page);
            List<AtmEntity> atm = page.getList();
            model.addAttribute("atm",atm);
            model.addAttribute("url","atm");


        } catch (Exception e) {
            e.printStackTrace();
        }
        return "list";


    }

    @RequestMapping("/A")
    public Object A() {

        return "A";

    }
    @RequestMapping("/B")
    public Object B() {

        return "B";

    }


}
