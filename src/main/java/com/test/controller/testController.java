//package com.test.controller;
//
//
//import com.test.entity.BookEntity;
//import com.test.entity.Page;
//import com.test.service.PageService;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
//
//@Controller
//public class testController {
//
//    private PageService ps = new PageService();
//
//    @RequestMapping(value = "/showAll.do")
//    public String findAllCourse(HttpServletRequest request,
//                                HttpServletResponse response) {
//                try {
//                    String pageNo = request.getParameter("pageNo");
//                    if (pageNo == null) {
//                        pageNo = "1";
//                    }
//                    Page page = ps.queryForBookPage(Integer.valueOf(pageNo), 10);
//                    request.setAttribute("page", page);
//                    List<BookEntity> course = page.getList();
//                    request.setAttribute("book", course);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                    return "list";
//    }
//}
