package com.test.entity;

import javax.persistence.*;


@Entity
@Table(name = "atm", schema = "aaa", catalog = "")
public class AtmEntity {
    private int id;
    private String name;
    private String password;
    private int money;

    public AtmEntity() {
    }

    public AtmEntity(int id, String name, String password, int money) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.money = money;
    }




    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "money")
    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AtmEntity atmEntity = (AtmEntity) o;

        if (id != atmEntity.id) return false;
        if (money != atmEntity.money) return false;
        if (name != null ? !name.equals(atmEntity.name) : atmEntity.name != null) return false;
        if (password != null ? !password.equals(atmEntity.password) : atmEntity.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + money;
        return result;
    }

    @Override
    public String toString() {
        return "AtmEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", money=" + money +
                "}\n";
    }
}
