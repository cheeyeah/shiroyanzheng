package com.test.entity;

import javax.persistence.*;


@Entity
@Table(name = "book", schema = "userdb_java", catalog = "")
public class BookEntity {
    private int id;

    public BookEntity() {
    }

    public BookEntity(int id, String name, int phone, int qq) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.qq = qq;
    }

    private String name;
    private int phone;
    private int qq;

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone=" + phone +
                ", qq=" + qq +
                "}\n";
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "phone")
    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "qq")
    public int getQq() {
        return qq;
    }

    public void setQq(int qq) {
        this.qq = qq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookEntity that = (BookEntity) o;

        if (id != that.id) return false;
        if (phone != that.phone) return false;
        if (qq != that.qq) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + phone;
        result = 31 * result + qq;
        return result;
    }
}
