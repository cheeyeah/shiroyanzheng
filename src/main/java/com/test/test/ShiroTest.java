package com.test.test;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ShiroTest {


    @Test
    public void test() {
        Factory<org.apache.shiro.mgt.SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
        org.apache.shiro.mgt.SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            UsernamePasswordToken upt = new UsernamePasswordToken("a", "b");

            try {
                subject.login(upt);

            } catch (UnknownAccountException e) {

                System.out.println("用户名不存在");
            } catch (IncorrectCredentialsException e) {
                System.out.println("密码错误");

            }

        }

        System.out.println(subject.getPrincipal());
        System.out.println(subject.hasRole("admin"));
        System.out.println(subject.hasRole("user"));
        subject.logout();
    }
}
