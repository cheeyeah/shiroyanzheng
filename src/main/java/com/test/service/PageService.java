package com.test.service;

import com.test.dao.AtmDao;
import com.test.dao.BookDao;
import com.test.entity.AtmEntity;
import com.test.entity.BookEntity;
import com.test.entity.Page;
import java.util.List;


public class PageService {


    public Page queryForAtmPage(int currentPage, int pageSize) {

        Page page = new Page();
        AtmDao atmDao=new AtmDao();
        //总记录数
        int allRow = atmDao.getAllRowCount();
        //当前页开始记录
        int offset = page.countOffset(currentPage, pageSize);
        //分页查询结果集
        List<AtmEntity> list = atmDao.query(offset, pageSize);

        page.setPageNo(currentPage);
        page.setPageSize(pageSize);
        page.setTotalRecords(allRow);
        page.setList(list);

        return page;
    }


    public Page queryForBookPage(int currentPage, int pageSize) {
        Page page = new Page();
        BookDao bookDao=new BookDao();
        //总记录数
        int allRow = bookDao.getAllRowCount();
        //当前页开始记录
        int offset = page.countOffset(currentPage, pageSize);
        //分页查询结果集
        List<BookEntity> list = bookDao.query(offset, pageSize);

        page.setPageNo(currentPage);
        page.setPageSize(pageSize);
        page.setTotalRecords(allRow);
        page.setList(list);

        return page;
    }
}