<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"%>
<%@include file="taglib.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="${ctx}/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/css/bootstrap-theme.min.css" >
    <script type="text/javascript" src="${ctx}/static/js/bootstrap.js"></script>
</head>
<body>
<h1 style="text-align: center">main page</h1>


<table style="margin: auto" border="1" width="70%">
    <tr>
        <td>id</td>
        <td>name</td>
        <td>password</td>
        <td>money</td>

    </tr>

    <c:forEach var="i" items="${atm}">
    <tr>
        <td>${ i.id }</td>
        <td>${ i.name }</td>
        <td>${ i.password }</td>
        <td>${ i.money }</td>
    <tr>
        </c:forEach>
    <c:forEach var="i" items="${book}">
    <tr>
        <td>${ i.id }</td>
        <td>${ i.name }</td>
        <td>${ i.phone }</td>
        <td>${ i.qq }</td>
    <tr>
        </c:forEach>

    <tr >
        <td colspan="6" align="center" bgcolor="#5BA8DE">共${page.totalRecords}条记录 共${page.totalPages}页 当前第${page.pageNo}页<br>

            <a href="${url}?pageNo=${page.topPageNo }"><input type="button" name="fristPage" value="首页" /></a>
            <c:choose>
                <c:when test="${page.pageNo!=1}">

                    <a href="${url}?pageNo=${page.previousPageNo }"><input type="button" name="previousPage" value="上一页" /></a>

                </c:when>
                <c:otherwise>

                    <input type="button" disabled="disabled" name="previousPage" value="上一页" />

                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${page.pageNo != page.totalPages}">
                    <a href="${url}?pageNo=${page.nextPageNo }"><input type="button" name="nextPage" value="下一页" /></a>
                </c:when>
                <c:otherwise>

                    <input type="button" disabled="disabled" name="nextPage" value="下一页" />

                </c:otherwise>
            </c:choose>
            <a href="${url}?pageNo=${page.bottomPageNo }"><input type="button" name="lastPage" value="尾页" /></a>
        </td>
    </tr>
</table>


</body>
</html>
