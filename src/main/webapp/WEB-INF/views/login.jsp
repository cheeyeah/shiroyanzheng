<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<shiro:guest>游客</shiro:guest>
<shiro:authenticated>

    <shiro:principal></shiro:principal>

</shiro:authenticated>
<form action="logindo" enctype="multipart/form-data">
    <input name="name" placeholder="name">
    <input name="password" placeholder="password">
    <h1>${message}</h1>
    <button class="btn btn-default">登录</button>
</form>
</body>
</html>
